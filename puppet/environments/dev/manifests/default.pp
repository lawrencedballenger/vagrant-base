class { 'epel': }

class { 'firewalld': }

class { '::letsencrypt':
  configure_epel => true,
  config         => {
    email  => 'webmaster@ldballenger.com',
    #server => 'https://acme-v01.api.letsencrypt.org/directory',
    server => 'https://acme-staging.api.letsencrypt.org/directory' # Test API
  },
  require        => Class['firewalld'],
}
letsencrypt::certonly { $site_domain :
  manage_cron          => true,
  cron_before_command  => 'systemctl stop nginx',
  cron_success_command => 'systemctl restart nginx',
}

file { '/etc/ssl/ssl_cert.pem':
  ensure => 'link',
  target => "/etc/letsencrypt/live/${site_domain}/fullchain.pem",
  mode   => '0600',
}
->file { '/etc/ssl/ssl_key.pem':
  ensure => 'link',
  target => "/etc/letsencrypt/live/${site_domain}/privkey.pem",
  mode   => '0600',
  before => Class['nginx'],
}

file { '/var/www/':
  ensure => directory,
  mode   => '0644',
}
->file { '/var/www/favicon.ico':
  ensure => file,
  source => "/vagrant/puppet/environments/${puppet_env}/manifests/files/favicon.ico",
  mode   => '0644',
}
->file { '/var/www/errors/':
  ensure => directory,
  mode   => '0644',
}
->file { '/var/www/errors/404.html':
  ensure => file,
  source => "/vagrant/puppet/environments/${puppet_env}/manifests/files/null.html",
  mode   => '0644',
}
->file { '/var/www/errors/50x.html':
  ensure => file,
  source => "/vagrant/puppet/environments/${puppet_env}/manifests/files/null.html",
  mode   => '0644',
}
->file { '/var/www/errors/index.html':
  ensure => file,
  source => "/vagrant/puppet/environments/${puppet_env}/manifests/files/null.html",
  mode   => '0644',
}
->file { '/var/www/index.html':
  ensure => file,
  source => "/vagrant/puppet/environments/${puppet_env}/manifests/files/null.html",
  mode   => '0644',
}
#SSL Hardening cipherli.st
#File sourced by running following command:
#openssl dhparam -out /etc/nginx/dhparam.pem 4096
->file { '/etc/ssl/dhparam.pem':
  ensure => file,
  source => "/vagrant/puppet/environments/${puppet_env}/manifests/files/dhparam.pem",
  mode   => '0600',
  before => Class['nginx'],
}

class{'nginx':
  manage_repo     => true,
  package_source  => 'nginx-mainline',
  confd_purge     => true,
  http_access_log => 'off',
  gzip_min_length => '1000',
  gzip_proxied    => 'expired no-cache no-store private auth',
  gzip_types      => [
    'text/plain',
    'application/x-javascript',
    'text/xml',
    'text/css',
    'application/xml'
  ],
  mime_types      => {
    'application/atom+xml'                                                      => 'atom',
    'application/java-archive'                                                  => 'jar war ear',
    'application/javascript'                                                    => 'js',
    'application/json'                                                          => 'json',
    'application/mac-binhex40'                                                  => 'hqx',
    'application/manifest+json'                                                 => 'webmanifest',
    'application/msword'                                                        => 'doc',
    'application/octet-stream'                                                  => 'bin exe dll',
    'application/octet-stream '                                                 => 'deb',
    'application/octet-stream  '                                                => 'dmg',
    'application/octet-stream   '                                               => 'iso img',
    'application/octet-stream    '                                              => 'msi msp msm',
    'application/pdf'                                                           => 'pdf',
    'application/postscript'                                                    => 'ps eps ai',
    'application/rss+xml'                                                       => 'rss',
    'application/rtf'                                                           => 'rtf',
    'application/vnd.apple.mpegurl'                                             => 'm3u8',
    'application/vnd.google-earth.kml+xml'                                      => 'kml',
    'application/vnd.google-earth.kmz'                                          => 'kmz',
    'application/vnd.ms-excel'                                                  => 'xls',
    'application/vnd.ms-fontobject'                                             => 'eot',
    'application/vnd.ms-powerpoint'                                             => 'ppt',
    'application/vnd.oasis.opendocument.graphics'                               => 'odg',
    'application/vnd.oasis.opendocument.presentation'                           => 'odp',
    'application/vnd.oasis.opendocument.spreadsheet'                            => 'ods',
    'application/vnd.oasis.opendocument.text'                                   => 'odt',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'         => 'xlsx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document'   => 'docx',
    'application/vnd.wap.wmlc'                                                  => 'wmlc',
    'application/x-7z-compressed'                                               => '7z',
    'application/x-cocoa'                                                       => 'cco',
    'application/x-java-archive-diff'                                           => 'jardiff',
    'application/x-java-jnlp-file'                                              => 'jnlp',
    'application/x-makeself'                                                    => 'run',
    'application/x-perl'                                                        => 'pl pm',
    'application/x-pilot'                                                       => 'prc pdb',
    'application/x-rar-compressed'                                              => 'rar',
    'application/x-sea'                                                         => 'sea',
    'application/x-stuffit'                                                     => 'sit',
    'application/x-tcl'                                                         => 'tcl tk',
    'application/x-x509-ca-cert'                                                => 'der pem crt',
    'application/x-xpinstall'                                                   => 'xpi',
    'application/xhtml+xml'                                                     => 'xhtml',
    'application/xspf+xml'                                                      => 'xspf',
    'application/zip'                                                           => 'zip',

    'audio/midi'                                                                => 'mid midi kar',
    'audio/mpeg'                                                                => 'mp3',
    'audio/ogg'                                                                 => 'ogg',
    #'audio/x-m4a'                                                               => 'm4a',
    #'audio/x-realaudio'                                                         => 'ra',

    'font/woff'                                                                 => 'woff',
    'font/woff2'                                                                => 'woff2',

    'image/gif'                                                                 => 'gif',
    'image/jpeg'                                                                => 'jpeg jpg',
    'image/png'                                                                 => 'png',
    'image/svg+xml'                                                             => 'svg svgz',
    'image/tiff'                                                                => 'tif tiff',
    'image/vnd.wap.wbmp'                                                        => 'wbmp',
    'image/webp'                                                                => 'webp',
    'image/x-icon'                                                              => 'ico',
    'image/x-jng'                                                               => 'jng',
    'image/x-ms-bmp'                                                            => 'bmp',

    'text/css'                                                                  => 'css',
    'text/csv'                                                                  => 'csv',
    'text/html'                                                                 => 'html htm shtml',
    'text/mathml'                                                               => 'mml',
    'text/plain'                                                                => 'txt',
    'text/x-component'                                                          => 'htc',
    'text/xml'                                                                  => 'xml',
    'text/xsl'                                                                  => 'xsl',

    'video/3gpp'                                                                => '3gpp 3gp',
    'video/mp2t'                                                                => 'ts',
    'video/mp4'                                                                 => 'mp4',
    'video/mpeg'                                                                => 'mpeg mpg',
    'video/quicktime'                                                           => 'mov',
    'video/webm'                                                                => 'webm',
    'video/x-flv'                                                               => 'flv',
    'video/x-m4v'                                                               => 'm4v',
    'video/x-mng'                                                               => 'mng',
    'video/x-ms-asf'                                                            => 'asx asf',
    'video/x-ms-wmv'                                                            => 'wmv',
    'video/x-msvideo'                                                           => 'avi',
  }
}

# file { '/var/www/test/':
#   ensure  => directory,
#   source  => [
#     'file:/vagrant/puppet/modules/test'
#   ],
#   recurse => 'remote',
#   mode    => '0644',
#   owner   => 'nginx',
#   group   => 'nginx',
#   require => Package['nginx'],
#   notify  => Service['nginx'],
# }
